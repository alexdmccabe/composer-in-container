## 1.0.8

* Fix namespace in test.
* Auto-migrate phpunit.xml.

## 1.0.7

* Coding standards fixes.

## 1.0.6

* Allow PHP 8.

## 1.0.5

* Update badges.

## 1.0.4

* Fix @throws in docblock.
* Change required PHP version to 7.1.
* Ensure environment variable is not set in test.
* Add missing @param.
* Import RuntimeException to make PHPStorm happy.

## 1.0.3

* Credit Red Hat for development time.

## 1.0.2

* Fix PHPUnit code coverage output for GitLab.
* Add more information about development.
* Add more information about why you may want to use this project to README.md.

## 1.0.1

* Added links to table of contents in README.md.
* Consolidated badges in README.md.

## 1.0.0

* Add CODE_OF_CONDUCT.md.
* Add README.md.
* Set up GitLab CI.
* Set up PHPUnit.

## 0.2.3

* Set up phpcs.

## 0.2.2

* Update error message.

## 0.2.1

* Set up autoloader.

## 0.2.0

* Add exception type.
* Fix namespacing.

## 0.1.0

* Add ScriptHandler class.
* Initialize project.
