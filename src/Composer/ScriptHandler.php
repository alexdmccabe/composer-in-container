<?php

namespace ComposerInContainer\Composer;

use Composer\Plugin\PreCommandRunEvent;
use ComposerInContainer\Exception\NotInContainerException;

/**
 * Script for checking that Composer is being run in a container.
 */
class ScriptHandler {

  /**
   * The environment variable name to use.
   *
   * @var string
   */
  const VARIABLE_NAME = 'COMPOSER_IN_CONTAINER';

  /**
   * Checks if composer is running inside of a container.
   *
   * @param \Composer\Plugin\PreCommandRunEvent $event
   *   The event object for the currently running command.
   *
   * @throws \ComposerInContainer\Exception\NotInContainerException
   */
  public static function preInstallUpdateCommand(PreCommandRunEvent $event) {
    if (!getenv(static::VARIABLE_NAME)) {
      throw new NotInContainerException('Composer commands should be run from inside the container.');
    }
  }

}
