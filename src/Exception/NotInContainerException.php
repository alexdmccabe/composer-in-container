<?php

namespace ComposerInContainer\Exception;

/**
 * Exception thrown when Composer commands are not run in a container.
 */
class NotInContainerException extends \RuntimeException {

}
