## Contents of this file

* [Introduction](#introduction)
* [Project status](#project-status)
* [Requirements](#requirements)
* [Installation](#installation)
* [Maintainers](#maintainers)

## Introduction

This utility provides a Composer script that will test for the existence of an environment variable. If the environment variable does not exist, no Composer commands will work.

Because each developer's system is unique, running the same Composer commands on different host systems can produce different results, based on PHP version, PHP extensions, etc. The purpose of this project is to attempt to ensure that Composer commands are always being run in a container to rule out issues caused by differing environments.

This is only meant to prevent **accidentally** running commands outside of a container. This is **not** meant to protect against malicious actors.

Development sponsored by [Red Hat][red-hat].

[red-hat]: https://www.redhat.com/

## Project status

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](CODE_OF_CONDUCT.md)
[![pipeline status](https://gitlab.com/alexdmccabe/composer-in-container/badges/main/pipeline.svg)](https://gitlab.com/alexdmccabe/composer-in-container/-/commits/main)
[![coverage report](https://gitlab.com/alexdmccabe/composer-in-container/badges/main/coverage.svg)](https://gitlab.com/alexdmccabe/composer-in-container/-/commits/main)

## Requirements

* PHP 7.3+

### Development requirements

* drupal/coder for coding standards testing
* phpunit/phpunit for unit testing

## Installation

* Install with composer: `composer require alexdmccabe/composer-in-container`.
* Add the script to the root composer.json of your project:
```json
{
  "scripts": {
    "pre-command-run": [
      "ComposerInContainer\\Composer\\ScriptHandler::preInstallUpdateCommand"
    ]
  }
}
```
* Add `COMPOSER_IN_CONTAINER=1` to the environment variables for the container that Composer will be run in.

## Maintainers

* Alex McCabe (alexdmccabe) - https://gitlab.com/alexdmccabe
