## Code of Conduct

Thank you for your interest in contributing to this project! By participating in this project, you agree to abide by the [code of conduct][coc].

[coc]: CODE_OF_CONDUCT.md

## Coding standards

This project uses the [Drupal project's][drupal] coding standards:

[drupal]: https://www.drupal.org/

* [Coding standards][coding-standards]
* [API documentation and comment standards][api-documentation-comment-standards]
* [Object-oriented code][object-oriented-code]

[coding-standards]: https://www.drupal.org/docs/develop/standards/coding-standards
[api-documentation-comment-standards]: https://www.drupal.org/docs/develop/standards/api-documentation-and-comment-standards
[object-oriented-code]: https://www.drupal.org/docs/develop/standards/object-oriented-code
