<?php

namespace ComposerInContainer\Tests\Unit;

use Composer\Plugin\PreCommandRunEvent;
use ComposerInContainer\Composer\ScriptHandler;
use ComposerInContainer\Exception\NotInContainerException;
use PHPUnit\Framework\TestCase;

/**
 * Tests the ScriptHandler class.
 *
 * @coversDefaultClass \ComposerInContainer\Composer\ScriptHandler
 *
 * @group composer_in_container
 */
class ScriptHandlerTest extends TestCase {

  /**
   * An event mock.
   *
   * @var \Composer\Plugin\PreCommandRunEvent|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $event;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $this->event = $this->getMockBuilder(PreCommandRunEvent::class)
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Tests preInstallUpdateCommand without ScriptHandler::VARIABLE_NAME set.
   *
   * @covers ::preInstallUpdateCommand
   */
  public function testPreInstallUpdateCommandNoValue() {
    putenv(ScriptHandler::VARIABLE_NAME);
    $this->expectException(NotInContainerException::class);
    ScriptHandler::preInstallUpdateCommand($this->event);
  }

  /**
   * Tests preInstallUpdateCommand with ScriptHandler::VARIABLE_NAME set to 0.
   *
   * @covers ::preInstallUpdateCommand
   */
  public function testPreInstallUpdateCommandValueZero() {
    putenv(ScriptHandler::VARIABLE_NAME . '=0');
    $this->expectException(NotInContainerException::class);
    ScriptHandler::preInstallUpdateCommand($this->event);
  }

  /**
   * Tests preInstallUpdateCommand with ScriptHandler::VARIABLE_NAME set to 1.
   *
   * @covers ::preInstallUpdateCommand
   */
  public function testPreInstallUpdateCommandValueOne() {
    putenv(ScriptHandler::VARIABLE_NAME . '=1');
    ScriptHandler::preInstallUpdateCommand($this->event);
    $this->assertEquals(TRUE, TRUE);
  }

}
